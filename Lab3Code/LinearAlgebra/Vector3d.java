/**
 *author: Amina Turdalieva 
 * id: 2035572
 * date: 2021-09-07
 */

 package LinearAlgebra;

public class Vector3d {
    
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX (){
        return this.x;
    }

    public double getY (){
        return this.y;
    }

    public double getZ (){
        return this.z;
    }

    public double magnitude() {  
        double magnitude = Math.sqrt( Math.pow(this.x,2) + Math.pow(this.y,2) + Math.pow(this.z,2) );
        return magnitude;
    }

    public double dotProduct(Vector3d vectorInput) {  
        double dotProduct = (this.x * vectorInput.getX()) + (this.y * vectorInput.getY()) + (this.z * vectorInput.getZ()) ;
        return dotProduct;
    }
 
    public Vector3d add(Vector3d vectorInput) {  
        double newX = this.x + vectorInput.getX();
        double newY = this.y + vectorInput.getY();
        double newZ = this.z + vectorInput.getZ();
        Vector3d newVector = new Vector3d(newX, newY, newZ);
        return newVector;
    }
}
