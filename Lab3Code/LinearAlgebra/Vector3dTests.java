/**
 *author: Amina Turdalieva 
 * id: 2035572
 * date: 2021-09-07
 */

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class Vector3dTests {

    @Test
    public void testVectorGetX() {
        Vector3d vector = new Vector3d(1,2,3);
        assertEquals(1, vector.getX());
    }

    @Test
    public void testVectorGetY() {
        Vector3d vector = new Vector3d(5,8,9);
        assertEquals(8, vector.getY());
    }

    @Test
    public void testVectorGetZ() {
        Vector3d vector = new Vector3d(3,6,12);
        assertEquals(12, vector.getZ());
    }

    @Test
    public void testMagnitude() {
        Vector3d vector = new Vector3d(1,2,3);
        assertEquals(Math.sqrt(14), vector.magnitude());
    }

    @Test
    public void testDotProduct() {
        Vector3d vector1 = new Vector3d(1,2,3);
        Vector3d vector2 = new Vector3d(4,5,6);
        assertEquals(32, vector1.dotProduct(vector2));
    }

    
    @Test
    public void testAdd() {
        Vector3d vector1 = new Vector3d(1,2,3);
        Vector3d vector2 = new Vector3d(4,5,6);
        Vector3d vector3 = vector1.add(vector2);
        assertEquals(5, vector3.getX());
        assertEquals(7, vector3.getY());
        assertEquals(9, vector3.getZ());
 
    }
}
